import requests
import base64
import json


class ZendeskBase:
    connection = {}
    _auth_header = None

    @property
    def auth_header(self):
        if self._auth_header:
            return self._auth_header

        if not self.connection.get("url"):
            raise Exception("Please provide Url to your Zendesk instance.")

        if not self.connection.get("username"):
            raise Exception("Please provide username for Zendesk API authentication.")

        if not self.connection.get("token"):
            raise Exception("Please provide API token for Zendesk API authentication.")

        creds = f"{self.connection['username']}/token:{self.connection['token']}"
        b64auth = base64.standard_b64encode(creds.encode("utf-8"))
        self._auth_header = {"Authorization": "Basic %s" % b64auth.decode()}

        return self._auth_header

    def lookup_user(self, email):
        print(f"Looking up user {email}")
        lookup_url = (
            f"{self.connection['url']}/api/v2/users/search.json?query=email:{email}"
        )
        response = requests.get(lookup_url, headers=self.auth_header)
        response.raise_for_status()
        users = response.json().get("users")
        if not len(users) == 1:
            raise Exception(
                f"Failure during user lookup, found {len(users)} expected 1"
            )
        user_id = users[0]["id"]
        print(f"Id: {user_id}")
        return user_id


class GetTicketInfo(ZendeskBase):
    ticketid = None

    def run(self):
        if not self.ticketid:
            raise Exception("Ticket ID required for retrieving information")

        url = f"{self.connection['url']}/api/v2/tickets/{self.ticketid}.json"

        print(
            f"Retrieving ticket information for ticket {self.ticketid} from Zendesk {url}"
        )

        response = requests.get(url, headers=self.auth_header)
        response.raise_for_status()

        return response.json()


class CreateTicket(ZendeskBase):
    subject = None
    comment = None
    public = False
    assignee = None
    tags = None

    def run(self):
        if not self.subject:
            raise Exception("Provide a subject for the ticket.")

        if not self.comment:
            raise Exception("Please provide a comment for the ticket.")

        url = f"{self.connection['url']}/api/v2/tickets"

        data = {"ticket": {}}
        if self.subject:
            data["ticket"]["subject"] = self.subject
        if self.comment:
            data["ticket"]["comment"] = {"body": self.comment, "public": self.public}
        if self.assignee:
            assignee_id = self.lookup_user(self.assignee)
            data["ticket"]["assignee_id"] = assignee_id
        if self.tags:
            data["ticket"]["tags"] = self.tags

        print(f"Creating Zendesk Ticket for {self.subject}")
        response = requests.post(url, headers=self.auth_header, json=data)
        response.raise_for_status()
        print(f"Successfully created {response.json().get('ticket').get('url')}")

        return response.json()


class UpdateTicket(ZendeskBase):
    ticket_id = None
    status = None
    subject = None
    comment = None
    public = False
    assignee = None
    tags = None

    def run(self):
        if not self.ticket_id:
            raise Exception("Please provide a Zendesk ticket ID.")

        if not self.status == "(Unchanged)":
            status = self.status

        url = f"{self.connection['url']}/api/v2/tickets/{self.ticket_id}.json"

        data = {"ticket": {}}
        if status:
            data["ticket"]["status"] = status
        if self.subject:
            data["ticket"]["subject"] = self.subject
        if self.comment:
            data["ticket"]["comment"] = {"body": self.comment, "public": self.public}
        if self.assignee:
            assignee_id = self.lookup_user(self.assignee)
            data["ticket"]["assignee_id"] = assignee_id
        if self.tags:
            data["ticket"]["tags"] = self.tags

        print(f"Updating Zendesk Ticket {self.ticket_id}")
        print(f"with {data}")
        response = requests.put(url, headers=self.auth_header, json=data)
        response.raise_for_status()
        print("Successfully updated ticket")

        return response.json()
